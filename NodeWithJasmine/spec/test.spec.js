
var app = require('./calculator') //on fait appel au fichier qui contient la fonction que nous voulons tester. 

describe("addition", function(){ // on défini le nomde la fonction de test
  it("should return the addition of two number", function(){ // on décrit ce que fait ce test
    var value  = app.add(4,6); //on utilise la fonction telle que définie dans le fichier calculator

    expect(value).toBe(10);  //on vérifie si le résultat attendu est le bon.
  });
});

describe("substraction", function () { // on défini le nomde la fonction de test
  it("should return the substraction of two number", function () { // on décrit ce que fait ce test
    var value = app.substract(6, 4); //on utilise la fonction telle que définie dans le fichier calculator

    expect(value).toBe(2);  //on vérifie si le résultat attendu est le bon.
  });
});

describe("multiply", function () { // on défini le nomde la fonction de test
  it("should return the multiplication of two number", function () { // on décrit ce que fait ce test
    var value = app.multiply(4, 6); //on utilise la fonction telle que définie dans le fichier calculator

    expect(value).toBe(24);  //on vérifie si le résultat attendu est le bon.
  });
});

